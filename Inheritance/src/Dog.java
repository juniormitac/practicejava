public class Dog extends Animal {

    private int eyes;
    private int legs;
    private int teeth;
    private int tail;
    private String coat;


    public Dog(int brain, int body, int size, int weight, String name, int eyes,
               int legs, int tail, int teeth,String coat) {
        super(brain, 1, size, weight, name);

        this.eyes = eyes;
        this.legs = legs;
        this.tail = tail;
        this.teeth = teeth;
        this.coat = coat;
    }



    private void chew(){
        System.out.println("dog.chew() called");
        eat();
    }

//    @Override
//    @Override
//    public void eat(int a){
//
//
//    }

    public void walk(){
        System.out.println("dog has walked");
        move(2);

    }

    public void run (){
        System.out.println("dog is running");
        move (4);
    }

//    @Override
//    public void move(int speed){
//        System.out.println("dog is moving");
//    }

    public int getEyes() {
        return eyes;
    }

    public int getLegs() {
        return legs;
    }
    public int getTeeth() {
        return teeth;
    }

    public int getTail() {
        return tail;
    }

    public String getCoat() {
        return coat;
    }

  
}
