public class Account {

private int accountNumber ;
private double balance;
private String customerName;
private String customerEmail;
private String phoneNumber;

public Account(){
    this(0,0.0,"Default Name","Default Email", "No number");
    System.out.println("empty constructor ");
}

public Account (int accountNumber, double balance, String customerName , String customerEmail, String phoneNumber){
    this.accountNumber=accountNumber;
    this.balance = balance;
    this.customerName=customerName;
    this.customerEmail=customerEmail;
    this.phoneNumber=phoneNumber;
}


public int getAccountNumber(){
    return accountNumber;
}

public double getBalance(){
    return balance;
}

public String getCustomerName(){
    return customerName;
}

public String getCustomerEmail(){
    return customerEmail;
}

public String getPhoneNumber(){
    return phoneNumber;
}

public void setAccountNumber(int accountNumber){
    this.accountNumber=accountNumber;
}

public void setBalance(double balance){
    this.balance = balance;
}

public void setCustomerName(String customerName){
    this.customerName = customerName;
}

public void setCustomerEmail(String customerEmail){
    this.customerEmail=customerEmail;
}

public void setPhoneNumber(String phoneNumber){
    this.phoneNumber=phoneNumber;
}

public void depositFunds(double deposit){
    this.balance = this.balance + deposit;
    System.out.println("New balance is: "+ balance);

}

public void withdrawFunds(double withdraw){



    if (this.balance - withdraw < 0)
        System.out.println("Not enough funds! ");

                  else this.balance = this.balance - withdraw;



}



}
